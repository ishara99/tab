
<!-- <button class="open-button" onclick="openForm()">Chat</button> -->
<a onclick="openForm()" class="float"><i class="fa fa-plus my-float"></i><sup>&nbsp;<span id="alertAll" style="color:red"></span></sup></a>

<div class="chat-popup col col-lg-5 col-sm-7" id="ChatPopup">
    <div class="container-md">
        <div class="row align-items-center">
            <div id="chatTypeWrap">
                <div class="col-6">
                    <button class="btn btn-primary btn-lg" id="PrivateButton">Private Chat <sup><span id="alertPrivate" style="color:red"></span></sup></button>
                    <button class="btn btn-primary btn-lg" id="groupchatButton">Group Chat <sup><span id="alertGroup" style="color:red"></span></sup></button>
                    <button class="btn btn-primary btn-lg" id="NotificationButton">Notification <sup><span id="alertNotification" style="color:red"></span></sup></button>


                    <button class="btn btn-danger btn-sm pull-right" onclick="closeForm()" aria-label="Close"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <hr>
        </div>
        <div class="row top-buffer"></div>
        <div class="row align-items-center">
            <div id="notificationWrap">
                <form id="Notificationform" action="">
                    <div class="notifiwrap" id="notificwrap">
                        <div name="oldnotificationtxt" id="oldnotificationtxt"></div>
                        <div name="notificationtxt" id="notificationtxt"></div>
                        <div name="notificationError" id="notificationError" class="errormsgchat"></div>
                    </div>
                    <div class="col-lg-9 col-sm-9" style="margin: 0 !important; padding: 0 !important">
                        <textarea id="Notificationinput" class="form-control customnotifiInput" autocomplete="off" rows="2" ></textarea>
                    </div>
                    <div class="col-lg-1 col-sm-1" style="margin: 0 !important; padding: 0 !important">
                        <button class="btn btn-primary buttonStylesendcusNot"><i class="fa fa-paper-plane-o fa-lg" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div id="ChatWrap">
                <div class="card col-sm-5 col-lg-5 align-self-start customReg" id="customReg">
                    <div id="RegisteredList" class="registeredList"></div>
                    <!-- <div id="vertLine" class="vl"></div> -->
                </div>

                <div class="col-sm-7 col-lg-7" style="background-color: white; margin-right: 0 !important; padding: 0 10px 0 0 !important">
                    <div class="chatbody">
                        <label class="chatPerson" id="PersonChat"></label>
                        <div id="chatPublicDiv">
                            <div id="chatDiv"></div>
                            <div class="messagedivclspub" id="messagesWrap">
                                <div class="sub col-lg-12 col-sm-8 col-md-8 col-xs-8">
                                    <div id="oldermsg"></div>
                                    <div id="messages"></div>
                                    <div id="errorsinChat" class="errormsgchat"></div>
                                </div>
                            </div>
                            <div id="SendGroupmsgWrap" class="sendmessageWrap">
                                <input type="hidden" name="usernameid" id="usernameid" value="<?php echo session()->get('username');  ?>">
                                <form id="Messageform" action="">
                                    <div class="row chatboxGroup">
                                        <div class="col-lg-10 col-sm-10" style="margin: 0 !important; padding: 0 !important">
                                            <input id="input" class="form-control custominput" autocomplete="off" />
                                        </div>
                                        <div class="col-lg-1 col-sm-1" style="margin: 0 !important; padding: 0 !important">
                                            <button class="btn btn-primary buttonStylesendcus"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="messagedivcls" id="PrivatemessagesWrap">
                                <div class="sub col-3">
                                    <div id="prvivateoldermsg"></div>
                                    <div id="privatemessages"></div>
                                    <div id="errorsinChat" class="errormsgchat"></div>
                                </div>
                            </div>
                            <div id="SendPrivatemsgWrap" class="sendmessageWrap">
                                <input type="hidden" name="usernameid" id="usernameid" value="<?php echo session()->get('username');  ?>">
                                <form id="MessageformPrivate" action="">
                                    <div class="row chatbox">
                                        <div class="col-lg-10 col-sm-10" style="margin: 0 !important; padding: 0 !important">
                                            <input id="inputPrivate" class="form-control custominput" autocomplete="off" />
                                        </div>
                                        <div class="col-lg-1 col-sm-1" style="margin: 0 !important; padding: 0 !important">
                                            <button class="btn btn-primary buttonStylesendcus"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Error Modal -->
<div class="modal fade" id="ErrorUserModal" tabindex="-1" role="dialog" aria-labelledby="errorUserModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="errorUserModalLabel">Error!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="color:red; text-align: center;" id="errorUser"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

{{--<script src="http://192.168.10.8:3000/socket.io/socket.io.js"></script>--}}
<script src="http://{{ Request::getHost() }}:3000/socket.io/socket.io.js"></script>

</div>

<div class="container-fluid footer_style">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style=" padding-right: 0px!important; padding-left: 0px;">
        <div class=" footer-copy-right">
            <p>© iPhonik (Pvt)Ltd.</p>
        </div>
    </div>
</div>

<script>
    function openForm() {
        document.getElementById("ChatPopup").style.display = "block";
        $("#alertAll").removeClass('dot');
        $("#notificationWrap, #PrivatemessagesWrap, #PersonChat, #ChatWrap").css('display', 'none');
    }

    function closeForm() {
        document.getElementById("ChatPopup").style.display = "none";
        $("#alertAll").removeClass('dot');
    }
</script>

<script>

    $(document).ready(function () {

        $("#ChatWrap").hide();
        $("#notificationWrap").hide();

        var msgWrap = document.getElementById("messagesWrap");
        msgWrap.scrollTop = msgWrap.scrollHeight;

        var notele = document.getElementById("notificwrap");
        notele.scrollTop = notele.scrollHeight;

        var pvtEle = document.getElementById("PrivatemessagesWrap");
        pvtEle.scrollTop = pvtEle.scrollHeight;

    });

    const socket = io(document.location.protocol+'//'+document.location.host+':3000', {transports: ['websocket']}, { autoConnect: false });
    const messageContainer = document.getElementById('messages');
    const pvtmessageContainer = document.getElementById('privatemessages');
    const oldmessageContainer = document.getElementById('oldermsg');
    const pvtoldmessageContainer = document.getElementById('prvivateoldermsg');
    const chatDiv = document.getElementById('chatDiv');
    const registeredList = document.getElementById('RegisteredList');
    const messageForm = document.getElementById('Messageform');
    const messageFormPrivate = document.getElementById('MessageformPrivate');
    const messageInput = document.getElementById('input');
    const messageInputPrivate = document.getElementById('inputPrivate');
    const name = document.getElementById('usernameid').value;

    // Notification Elements

    const notificationform = document.getElementById('Notificationform');
    const notificationContainer = document.getElementById('notificationtxt');
    const oldnotificationContainer = document.getElementById('oldnotificationtxt');
    const notificationinput = document.getElementById('Notificationinput');


    appendMessage('You joined')
    socket.emit('new-user', name, function(data){

        if(!data){

            // $("#errorUser").text("The username is already Taken. Try another username!");
            // $('#ErrorUserModal').modal('toggle');

        }

    });

    socket.on('UserErrorRefresh', data => {

        // $("#errorUser").text(data);
        // $('#ErrorUserModal').modal('toggle');

    });


    var TypeChat = '';

    $( "#PrivateButton" ).click(function() {
        TypeChat = 'pvt';
        // alert(TypeChat);
        // $('#ChatWrap').show();
        $('#ChatWrap, #RegisteredList, #vertLine, #customReg').css('display', 'block');
        $("#notificationWrap, #PrivatemessagesWrap, #PersonChat, #SendGroupmsgWrap, #SendPrivatemsgWrap, #messagesWrap").css('display', 'none');
        $("#PrivateButton").addClass('active');
        $("#alertPrivate").removeClass('dot');
        // $('#chatTypeWrap').hide();

        socket.emit('chatType', TypeChat, function(data){

        });
        $("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());

    });

    $( "#groupchatButton" ).click(function() {
        TypeChat = 'pbl';
        // alert(TypeChat);
        $('#ChatWrap, #messagesWrap, #SendGroupmsgWrap').css('display', 'block');
        $("#notificationWrap, #PersonChat, #PrivatemessagesWrap, #RegisteredList, #vertLine, #customReg, #SendPrivatemsgWrap").css('display', 'none');
        $('#messages').empty();
        $('#oldermsg').empty();
        // $('#chatTypeWrap').hide();
        $("#alertGroup").removeClass('dot');
        $("#groupchatButton").addClass('active');

        socket.emit('chatType', TypeChat, function(data){

        });
        $("#messagesWrap").scrollTop($("#messagesWrap").height());
    });

    $( "#NotificationButton" ).click(function() {
        TypeChat = 'group';
        // alert(TypeChat);
        $("#notificationWrap").css('display', 'block');
        $('#notificationWrap').animate({scrollTop: 1000000}, 10);
        $('#ChatWrap').css('display', 'none');
        $('#notificationtxt').empty();
        $('#oldnotificationtxt').empty();
        $("#alertNotification").removeClass('dot');
        $("#NotificationButton").addClass('active');


        socket.emit('chatType', TypeChat, function(data){

        });

        // $("#notificwrap").scrollTop($("#notificwrap").height());
        $(window).scrollTop( $("#notificwrap").offset().top );

    });

    socket.on('usernames', function(data){

        $("#RegisteredList").empty();
        const list = document.createElement('list');

        for (var i = 0; i < data.length; i++) {

            var body = data[i];

            if (body !== name) {

                const li = document.createElement('li');
                const sup = document.createElement('sup');
                const span = document.createElement('span');
                span.setAttribute("id", body+"span");
                li.setAttribute("data-chat", body);
                li.setAttribute("class", "list-group-item customFormLi");
                li.setAttribute("id", body);
                li.innerHTML = body;
                sup.append(span);
                li.append(span);
                list.appendChild(li);

                (function(value){
                    li.addEventListener("click", function(){

                        socket.emit('send-user', value, function(data){

                        });


                        socket.emit('create', value);
                        
                        console.log("#"+value+"span");
                        $('#privatemessages').empty();
                        $('#prvivateoldermsg').empty();
                        $('#ChatWrap').show();
                        $('#PrivatemessagesWrap').show();
                        $('#SendPrivatemsgWrap').show();
                        $("#PersonChat").show();
                        $("#PersonChat").empty();
                        $("#PersonChat").append(value);


                        const chatDIVpvt = document.createElement('div');
                        chatDIVpvt.setAttribute("data-chat", body);
                        chatDIVpvt.setAttribute("id", body);
                        chatDIVpvt.setAttribute("class", "ChatUser");
                        messageContainer.append(chatDIVpvt);

                        $("#"+value+"span").removeClass("dot");

                    }, false)
                })(data[i]);
            }

        }

        registeredList.appendChild(list);

    })

    socket.on('olderNotifications', function(data) {
        // appendMessage(`${data.sender}: ${data.message}`)
        console.log(data);

        for (var i = 0; i < data.length; i++){
            // We store html as a var then add to DOM after for efficiency
            // html += '<li>' + oldMessages[i].message + '</li>'
            const divoldnot = document.createElement('div');

            if(data[i].publisher == name){

                divoldnot.innerHTML = data[i].notification +"<br/>( From: You, To: "+data[i].receiver+" )";
                divoldnot.setAttribute("class", "btn btn-info customNotification");

            }else{


                divoldnot.innerHTML = data[i].notification + "<br/>( From " +data[i].publisher+" )";
                divoldnot.setAttribute("class", "btn btn-info customNotification");

            }

            oldnotificationContainer.append(divoldnot);
            $("#notificwrap").scrollTop($("#notificwrap").height());
        }

    });




    notificationform.addEventListener('submit', e => {
        e.preventDefault();

        const notificationmsg = notificationinput.value;

        if(notificationmsg.length !== 0 || notificationmsg.trim() !== ""){

            const notificationElement = document.createElement('div');
            notificationElement.innerText = notificationmsg + " : From You";
            notificationElement.setAttribute("class", "btn btn-info customNotification");
            /*notificationContainer.append(notificationElement);*/

        }

        socket.emit('send-notification', notificationmsg, function(data) {
            // here errorsinChat
            console.log(data);
            if (data.length == 0) {

            }else{

                $('#notificationError').html(data);
                setTimeout(function () {
                    $('#notificationError').html('');
                }, 3000);
                $("#notificwrap").scrollTop($("#notificwrap").height());
            }


        })
        $("#notificwrap").scrollTop($("#notificwrap").height());
        notificationinput.value = ''

    });

    socket.on('notificationBroadcast', data => {

        $("#alertNotification").addClass("dot");
        $("#alertAll").addClass("dot");
        const notificationElement = document.createElement('div');
        notificationElement.innerText = `${data.notification} : From ${data.name}`;
        notificationElement.setAttribute("class", "btn btn-info customNotification");
        notificationContainer.append(notificationElement);
        $("#notificwrap").scrollTop($("#notificwrap").height());

    });

    socket.on('ApproveNotification', data => {

        console.log(data.appNotification);
        $("#alertNotification").addClass("dot");
        $("#alertAll").addClass("dot");
        const approveElement = document.createElement('div');
        approveElement.innerText = data.appNotification;
        approveElement.setAttribute("class", "btn btn-info customNotification");
        notificationContainer.append(approveElement);


        var endpoint = document.getElementById("endpoint").value;
        $.ajax({
            url: 'breakStart',
            type: 'GET',
            data: {endpoint: endpoint, brkreason: data.break_type},
            success: function(response) {
                location.href = "{{url ('Agentdashboard')}} ";
            }
        });

        $("#notificwrap").scrollTop($("#notificwrap").height());

    });

    socket.on('penaltyNotification', data => {

        // alert(data)
        $("#alertNotification").addClass("dot");
        $("#alertAll").addClass("dot");
        const penaltyElement = document.createElement('div');
        penaltyElement.innerText = data;
        penaltyElement.setAttribute("class", "btn btn-info customNotification");
        notificationContainer.append(penaltyElement);
        $("#notificwrap").scrollTop($("#notificwrap").height());

    });

    socket.on('rejetNotification', data => {

        // alert(data)
        $("#alertNotification").addClass("dot");
        $("#alertAll").addClass("dot");
        const rejectElement = document.createElement('div');
        rejectElement.innerText = data;
        rejectElement.setAttribute("class", "btn btn-info customNotification");
        notificationContainer.append(rejectElement);
        $("#notificwrap").scrollTop($("#notificwrap").height());

    });

    socket.on('RequestNotification', data => {

        // alert(data)
        $("#alertNotification").addClass("dot");
        $("#alertAll").addClass("dot");
        const requestElement = document.createElement('div');
        requestElement.innerText = data;
        requestElement.setAttribute("class", "btn btn-info customNotification");
        notificationContainer.append(requestElement);
        $("#notificwrap").scrollTop($("#notificwrap").height());

    });


    // socket.on('user-connected', name => {
    //  // const sessionID = socket.socket.sessionid;
    //  // console.log(sessionid);
    //      appendMessage(`${name} connected`)
    // })

    socket.on('olderMessages', function(data) {
        // appendMessage(`${data.sender}: ${data.message}`)
        console.log(data);

        for (var i = 0; i < data.length; i++){
            // We store html as a var then add to DOM after for efficiency
            // html += '<li>' + oldMessages[i].message + '</li>'
            const divold = document.createElement('div');

            if(data[i].sender == name){

                divold.innerHTML = data[i].message;
                divold.setAttribute("class", "row messageSendclass");

            }else{


                divold.innerHTML = data[i].message;
                divold.setAttribute("class", "row messagereplyclass");

            }

            pvtoldmessageContainer.append(divold);
        }
        $("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());

    });

    socket.on('olderMessagespublic', function(data) {
        // appendMessage(`${data.sender}: ${data.message}`)
        console.log(data);

        for (var i = 0; i < data.length; i++){
            // We store html as a var then add to DOM after for efficiency
            // html += '<li>' + oldMessages[i].message + '</li>'
            const divoldpub = document.createElement('div');

            if(data[i].sender == name){

                divoldpub.innerHTML = data[i].sender+": "+data[i].message;
                divoldpub.setAttribute("class", "row messageSendclass");

            }else{


                divoldpub.innerHTML = data[i].sender+": "+data[i].message;
                divoldpub.setAttribute("class", "row messagereplyclass");

            }

            oldmessageContainer.append(divoldpub);
        }
        $("#messagesWrap").scrollTop($("#messagesWrap").height());

    });


    socket.on('user-disconnected', function(data) {

        console.log(data);
        appendMessage(`${data} disconnected`)
    });



    messageForm.addEventListener('submit', e => {
        e.preventDefault();
        const message = messageInput.value;
        $("#alertPrivate").removeClass('dot');
        $("#alertGroup").removeClass('dot');


        if(message.length !== 0 || message.trim() !== ""){

            if(TypeChat == 'pbl'){

                appendMessage(`${message}`);

            }else if(TypeChat == 'pvt'){

                const meElement = document.createElement('div');
                meElement.innerText = `${message}`;
                meElement.setAttribute("class", "messageSendclass");
                pvtmessageContainer.append(meElement);
                $("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());
            }

        }

        socket.emit('send-chat-message', message, function(data) {
            // here errorsinChat
            console.log(data);
            if (data.length == 0) {

            }else{

                $('#errorsinChat').html(data);
                setTimeout(function () {
                    $('#errorsinChat').html('');
                }, 3000);
                $("#messagesWrap").scrollTop($("#messagesWrap").height());
            }


        });
        messageInput.value = ''
    });

    messageFormPrivate.addEventListener('submit', e => {
        e.preventDefault();
        const message = messageInputPrivate.value;
        $("#alertPrivate").removeClass('dot');
        $("#alertGroup").removeClass('dot');


        if(message.length !== 0 || message.trim() !== ""){

            if(TypeChat == 'pbl'){

                appendMessage(`${message}`);

            }else if(TypeChat == 'pvt'){

                const meElement = document.createElement('div');
                meElement.innerText = `${message}`;
                meElement.setAttribute("class", "messageSendclass");
                pvtmessageContainer.append(meElement);
                $("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());
            }

        }

        socket.emit('send-chat-message', message, function(data) {
            // here errorsinChat
            console.log(data);
            if (data.length == 0) {

            }else{

                $('#errorsinChat').html(data);
                setTimeout(function () {
                    $('#errorsinChat').html('');
                }, 3000);
                $("#messagesWrap").scrollTop($("#messagesWrap").height());
            }


        });
        messageInputPrivate.value = ''
    });

    socket.on('chat-message', data => {

        $("#alertGroup").addClass("dot");
        $("#alertAll").addClass("dot");

        // appendMessage(`${data.name}: ${data.message}`)
        const element = document.createElement('div');
        element.innerText = `${data.name}: ${data.message}`;
        element.setAttribute("class", "messagereplyclass");
        // messageContainer.append(element);

        $("#messagesWrap").scrollTop($("#messagesWrap").height());
    });

    socket.on('chat-message-private', function(data) {

        var user = data.name;
        $("#alertPrivate").addClass("dot");
        $("#alertAll").addClass("dot");
        $("#"+user+"span").addClass("dot");
        // appendMessage(`${data.name}: ${data.message}`)
        const messageEl = document.createElement('div');
        messageEl.innerText = `${data.message}`;
        // $(`#${data.name}`).append(messageEl);
        messageEl.setAttribute("class", "messagereplyclass");
        pvtmessageContainer.append(messageEl);
        console.log(messageEl);

        $("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());

    });

    function appendMessage(message) {
        const messageElement = document.createElement('div');
        messageElement.innerText = message;
        messageElement.setAttribute("class", "messageSendclass");
        messageContainer.append(messageElement);
        $("#messagesWrap").scrollTop($("#messagesWrap").height());
    }

</script>
 
  
