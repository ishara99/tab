 [from-amn]
exten => s,1,Answer()
exten => s,n,Verbose(${CALLERID(all)} calling ${EXTEN})
exten => s,n,MixMonitor(${UNIQUEID}.wav,ab)
exten => s,n,System(php /var/lib/asterisk/agi-bin/get_uid.php ${Camp} ${UNIQUEID} ${Number})
exten => s,n,Dial(SIP/${Number}@dialog)
exten => s,n,Hangup()


[call_rating]
exten => s,1,Answer()
exten => s,2,AGI(/var/lib/asterisk/agi-bin/RateCall.php,${UNIQUEID},${CALLERID(dnid)},${CALLERID(num)},${BLINDTRANSFER},${CALLERID(name)},${CALLERID(num)})
exten => s,3,Hangup()


[dnd-override]
exten => _X.,1,NoOp(####################This context overrides DND)
same => n,GotoIf(${MOB_NUMCHEK(${EXTEN})}?extention,${EXTEN},1:mobile,${EXTEN},1)

[mobile]
exten => _X.,1,noop(==GOTO MOBNUM==)
same => n,Goto(from-internal,${EXTEN},1)
same => n,Hangup()

[extention]
exten => _X.,1,NoOp(####chck_dnd###)
same => n,NoOp(NUMBER##### ${EXTEN})
same => n,GotoIf(${DND_STATUS(${EXTEN})}?dnd_off,${EXTEN},1:dnd_on,${EXTEN},1)

[dnd_off]
exten => _X.,1,Goto(from-internal,${EXTEN},1)
same => n,Hangup()

[dnd_on]
exten => _X.,1,noop(==DND Deactivated==)
same => n,hangup()


[from-conf]
exten => _X.,1,Progress()
same => n,Wait()
same => n,NoOp(${EXTEN})
same => n,ConfBridge(${EXTEN},default_bridge,default_user)

[from-spy]
exten => _X.,1,Progress()
same => n,Wait()
same => n,NoOp(${EXTEN})
same => n,ChanSpy(PJSIP/${EXTEN},qE)

[from-whisper]
exten => _X.,1,Progress()
same => n,Wait()
same => n,NoOp(${EXTEN})
same => n,ChanSpy(PJSIP/${EXTEN},qwE)

[from-barge]
exten => _X.,1,Progress()
same => n,Wait()
same => n,NoOp(${EXTEN})
same => n,ChanSpy(PJSIP/${EXTEN},qBE)



[from-queue-custom]
exten => _X.,1,Goto(dnd-override,${QAGENT},1)

exten => _.,1,Set(QAGENT=${EXTEN})
exten => _.,n,Set(__FROMQ=true)
exten => _.,n,NoOp(***********????????????????????*************)
exten => _.,n,GotoIf($["${LEN(${NODEST})}" = "0"]?hangup)
exten => _.,n,GotoIf($["${DIALPLAN_EXISTS(from-queue,${NODEST},1)}" = "1"]?${NODEST},1:hangup)
exten => _.,n(hangup),Macro(hangupcall,)



exten => h,1,Macro(hangupcall,)

;--== end of [from-queue] ==--;

